from rss import *


def test_generate_audio_filename():
    assert generate_audio_filename("GABI, A Nº1 DO MUNDO - Floresta dos Proplayers | AGE OF EMPIRES 4") == \
           'gabian1domundoflorestadosproplayersageofempires4.mp3'
