import requests
import pytube
import os
import json
import base64
import re
import logging
from podgen import Podcast, Episode, Media, Person


YT_KEY = os.getenv("YOUTUBE_TOKEN")
CHANNEL_ID = "UCJ0vp6VTn7JuFNEMj5YIRcQ"
EPISODES_ID = "31705528"
PODCAST_ID = "31705386"
GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")


logger = logging.getLogger("tomahawkaoe")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def is_podcast_episode(video):
    try:
        return "floresta dos proplayers" in str(video["items"][0]["snippet"]["title"]).lower() or \
               "podcast" in [it.lower() for it in video["items"][0]["snippet"]["tags"]]
    except KeyError:
        return False


def get_video_info(video_id):
    response = requests.get(f"https://www.googleapis.com/youtube/v3/videos?part=id%2C+snippet&id={video_id}&key={YT_KEY}")
    response.raise_for_status()
    return response.json()


def filter_yt_videos(videos):
    return [it for it in videos if is_podcast_episode(it)]


def list_audios_on_gitlab():
    headers = {"PRIVATE-TOKEN": GITLAB_TOKEN}
    response = requests.get(f"https://gitlab.com/api/v4/projects/{EPISODES_ID}/repository/tree",
                            headers=headers)
    response.raise_for_status()
    json_data = response.json()
    return [it.get("name") for it in json_data]


def list_all_videos_ids(channel_id):

    def _extract_videos_from_list(content):
        items = content["items"]
        return [it["id"]["videoId"] for it in items]

    videos = []
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.get(f"https://www.googleapis.com/youtube/v3/search?key={YT_KEY}&channelId={channel_id}&part=snippet,id&order=date&type=video", headers=headers)
    response.raise_for_status()
    videos.extend(_extract_videos_from_list(response.json()))
    while True:
        try:
            page_token = response.json()["nextPageToken"]
        except KeyError:
            break
        response = requests.get(f"https://www.googleapis.com/youtube/v3/search?key={YT_KEY}&channelId={channel_id}&part=snippet,id&order=date&maxResults=50&type=video&pageToken={page_token}", headers=headers)
        response.raise_for_status()
        videos.extend(_extract_videos_from_list(response.json()))
    return videos


def compare_entries(yt_entries, gitlab_entries):
    new_episodes = []
    filtered_entries = filter_yt_videos(yt_entries)
    for it in filtered_entries:
        if generate_audio_filename(it["items"][0]["snippet"]["title"]) not in gitlab_entries:
            new_episodes.append(it)
    return new_episodes


def get_audio_link(video):
    title = generate_audio_filename(video["items"][0]["snippet"]["title"])
    return f"https://gitlab.com/tomahawkaoe/episodes/-/raw/main/{title}"


def generate_rss_file(videos):
    filename = "tomahawkaoe.rss"
    podcast = Podcast(
        name="Tomahawk AoE",
        description="Tudo sobre Age of Empires",
        website="https://www.youtube.com/c/Tomahawkaoe",
        feed_url="https://gitlab.com/tomahawkaoe/podcast/-/raw/rss/tomahawk.rss",
        explicit=False,
    )
    owner = Person("Vitor Machado", "tomahawkaoe@gmail.com")
    podcast.owner = owner
    podcast.image = "https://gitlab.com/tomahawkaoe/podcast/-/raw/main/banner.jpg"
    podcast.authors = [owner]
    for it in videos:
        podcast.episodes.append(
            Episode(
                title=it["items"][0]["snippet"]["title"],
                media=Media(get_audio_link(it), os.path.getsize(generate_audio_filename(it["items"][0]["snippet"]["title"]))),
                summary=it["items"][0]["snippet"]["description"],
            )
        )
    podcast.rss_file(filename)
    return filename


def upload_rss_file(filename):
    data = open(filename, "rb").read()
    encoded = base64.b64encode(data)
    filename = os.path.basename(filename)
    payload = {
        "branch": "rss",
        "commit_message": f"[skip ci] Update RSS file {filename}",
        "actions": [
            {
                "action": "update",
                "file_path": "tomahawk.rss",
                "content": encoded.decode(),
                "encoding": "base64"
            }
        ]
    }
    payload = json.dumps(payload)
    headers = {"PRIVATE-TOKEN": GITLAB_TOKEN, "Content-Type": "application/json"}
    response = requests.post(f"https://gitlab.com/api/v4/projects/{PODCAST_ID}/repository/commits",
                             headers=headers, data=payload)
    response.raise_for_status()
    return response.json()


def download_audio_from_youtube(url, filename):
    yt = pytube.YouTube(url)
    video = yt.streams.get_audio_only()
    video.download(filename=filename)


def upload_audio_to_gitlab(filename):
    data = open(filename, "rb").read()
    encoded = base64.b64encode(data)
    filename = os.path.basename(filename)
    payload = {
        "branch": "main",
        "commit_message": f"Add {filename}",
        "actions": [
        {
          "action": "create",
          "file_path": filename,
          "content": encoded.decode(),
          "encoding": "base64"
        }
      ]
    }
    payload = json.dumps(payload)
    headers = {"PRIVATE-TOKEN": GITLAB_TOKEN, "Content-Type": "application/json"}
    response = requests.post(f"https://gitlab.com/api/v4/projects/{EPISODES_ID}/repository/commits",
                             headers=headers, data=payload)
    response.raise_for_status()
    return response.json()


def generate_audio_filename(filename):
    filename = str(filename).lower()
    return re.sub(r"[\W_\s]+", '', filename, flags=re.ASCII) + ".mp3"


def load_local_videos_info(filename):
    with open(filename) as json_file:
        return json.load(json_file)


def main():
    if not GITLAB_TOKEN:
        raise ValueError("GITLAB_TOKEN is missing.")

    if os.getenv("TOMAHAWKAOE_LOCAL"):
        videos = load_local_videos_info("yt_video_info_list.json")
    else:
        if not YT_KEY:
            raise ValueError("YOUTUBE_TOKEN is missing.")
        video_ids = list_all_videos_ids(CHANNEL_ID)
        logger.debug("Found {} videos in total.".format(len(video_ids)))
        videos = [get_video_info(it) for it in video_ids]

    podcast_videos = filter_yt_videos(videos)
    logger.debug("Found {} videos on YT for podcast.".format(len(podcast_videos)))
    uploaded_videos = list_audios_on_gitlab()
    logger.debug("There are {} audios available on Gitlab".format(len(uploaded_videos)))
    new_episodes = compare_entries(podcast_videos, uploaded_videos)
    logger.info("Found {} new podcast videos on YT".format(len(new_episodes)))
    for ep in new_episodes:
        ep_id = ep["items"][0]["id"]
        ep_name = generate_audio_filename(ep["items"][0]["snippet"]["title"])
        logger.info(f"New episode {ep_id}: {ep_name}")
        logger.debug(f"Downloading episode {ep_id}")
        download_audio_from_youtube(f"https://www.youtube.com/watch?v={ep_id}", ep_name)
        logger.debug(f"Uploading {ep_name} to Gitlab")
        upload_audio_to_gitlab(ep_name)
    if new_episodes or True:
        logger.debug("Generating RSS file")
        rss_file = generate_rss_file(podcast_videos)
        logger.debug("Uploading RSS file")
        upload_rss_file(rss_file)


if __name__ == "__main__":
    main()
